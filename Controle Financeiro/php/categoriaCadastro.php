<?php

    include ("conexao.php");

    $id = '';
    $descr = '';
    $tipo = '';
    
    if($_POST && $_REQUEST['act'] == "save"){
        if(isset($_POST['id']) && $_POST['id'] !== '')
            $id = $_POST['id'];
        else {
            $id = null;
        }
        $tipo       = $_POST['tipo'];
        $descr      = $_POST['descr'];
        $id_usuario = $_POST['id_usuario'];

        $variaveis = [
            'id'            => $id,
            'tipo'          => $tipo,
            'descr'         => $descr,
            'id_usuario'    => $id_usuario
        ];

        if($id == null){
            $sql = "INSERT INTO categoria (id,tipo,descr,id_usuario) VALUES (:id,:tipo,:descr,:id_usuario)";
        } else{
            $sql = "UPDATE categoria set descr = :descr, tipo = :tipo where id = :id and id_usuario = :id_usuario";
        }
        $pdo->prepare($sql)->execute($variaveis);
   
        //echo "<script>alert('Cadastro realizado com sucesso ')</script>";
        echo "<script>window.location='categoria.php'</script>";
    }
    else if(isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd"){
        $id = $_REQUEST["id"];
        try {
            $categorias = $pdo->prepare("SELECT * FROM categoria WHERE id = ?");
            $categorias->bindParam(1, $id, PDO::PARAM_INT);
            if ($categorias->execute()) {
                $campos = $categorias->fetch(PDO::FETCH_OBJ);
                $id = $campos->id;
                $descr = $campos->descr;
                $tipo = $campos->tipo;
            } else {
                throw new PDOException("Erro: Não foi possível executar a declaração sql");
            }
        } catch (PDOException $erro) {
            echo "Erro: ".$erro->getMessage();
    }
    }
     else if(isset($_REQUEST["act"]) && $_REQUEST["act"] == "del"){
        $id = $_REQUEST["id"];
        try {
            $categorias = $pdo->prepare("DELETE FROM categoria WHERE id = ?");
            $categorias->bindParam(1, $id, PDO::PARAM_INT);
            if ($categorias->execute()) {
                echo "<script>alert('Exclusão realizada com sucesso ')</script>";
                echo "<script>window.location='categoria.php'</script>";
            } else {
                throw new PDOException("Erro: Não foi possível executar a declaração sql");
            }
        } catch (PDOException $erro) {
            echo "Erro: ".$erro->getMessage();
    }
    }


?>