<?php

    include ("conexao.php");

    $ano = date("Y");
    $id = $_SESSION['id'];

     try {
             for($mes = 1; $mes < 13; $mes++){
                 $receitas = $pdo->prepare("SELECT SUM(movto.vltot) receitas FROM movto where movto.tipo = '1' and YEAR(dtven) = $ano and MONTH(dtven) = $mes and id_usuario = $id");
                 $receitas->execute();
                 $despesas = $pdo->prepare("SELECT SUM(movto.vltot) despesas FROM movto where movto.tipo = '2' and YEAR(dtven) = $ano and MONTH(dtven) = $mes and id_usuario = $id");
                 $despesas->execute();

                 $receitas = $receitas->fetch(PDO::FETCH_OBJ);
                 if($receitas->receitas == null)
                    $receitasMes[] = 0;
                 else
                    $receitasMes[] = $receitas->receitas;
                    
                 $despesas = $despesas->fetch(PDO::FETCH_OBJ);
                if($despesas->despesas == null)
                    $despesasMes[] = 0;
                 else
                    $despesasMes[] = $despesas->despesas;
             }

                $categoriaDespesas = $pdo->prepare("SELECT SUM(movto.vltot) despesas, categoria.descr, movto.id_categoria from movto left join categoria on categoria.id = movto.id_categoria where movto.tipo = '2' and YEAR(dtven) = $ano and movto.id_usuario = $id group by movto.id_categoria");
                $categoriaDespesas->execute();
                while($campos = $categoriaDespesas->fetch(PDO::FETCH_OBJ)){
                    $despesasCategoria[] = [$campos->descr,$campos->despesas];
                }
                if($campos){
                $qtdeCategorias = sizeof($despesasCategoria);
                }else{
                    $qtdeCategorias = 0;
                }

                $categoriaReceitas = $pdo->prepare("SELECT SUM(movto.vltot) receitas, categoria.descr, movto.id_categoria from movto left join categoria on categoria.id = movto.id_categoria where movto.tipo = '1' and YEAR(dtven) = $ano and movto.id_usuario = $id group by movto.id_categoria");
                $categoriaReceitas->execute();
                while($campos = $categoriaReceitas->fetch(PDO::FETCH_OBJ)){
                    $receitasCategoria[] = [$campos->descr,$campos->receitas];
                }
                if($campos){
                $qtdeCategorias = sizeof($receitasCategoria);
                }else{
                    $qtdeCategorias = 0;
                }

         } catch (PDOException $erro) {
             echo "Erro: ".$erro->getMessage();
         }
?>