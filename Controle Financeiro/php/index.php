<?php

    include ("conexao.php");

    $ano = date("Y");
    $id = $_SESSION['id'];

     try {
             $receitas = $pdo->prepare("SELECT SUM(movto.vltot) receitas FROM movto where movto.tipo = '1' and YEAR(dtemi) = $ano and id_usuario = $id");
             $receitas->execute();
             $receitas = $receitas->fetch(PDO::FETCH_OBJ);
             $receitaAnual = $receitas->receitas;       
             $despesas = $pdo->prepare("SELECT SUM(movto.vltot) despesas FROM movto where movto.tipo = '2' and YEAR(dtemi) = $ano and id_usuario = $id");
             $despesas->execute();
             $despesas = $despesas->fetch(PDO::FETCH_OBJ);
             $despesasAnual = $despesas->despesas;
             if($despesasAnual == 0){
                 $lucro = 100;
             }else if($receitaAnual == 0){
                $lucro = 0;
            }else{
             $lucro = (($receitaAnual - $despesasAnual) / $receitaAnual) * 100;
             }
             $lucro = number_format($lucro, 0);
         } catch (PDOException $erro) {
             echo "Erro: ".$erro->getMessage();
         }
?>