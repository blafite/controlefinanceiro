<?php

    include ("conexao.php");

    $id = '';
    $categoria = '';
    $dtven = '';
    $preco = '';
    $desconto = '';
    $acrescimo = '';
    $juros = '';
    $vltot = '';
    
    if($_POST && $_REQUEST['act'] == "save"){

        if(isset($_POST['id']) && $_POST['id'] !== '')
            $id = $_POST['id'];
        else {
            $id = null;
        }

        $id_usuario     = $_POST['id_usuario'];
        $tipo           = $_POST['tipo'];
        $categoria      = $_POST['categoria'];
        $dtven          = $_POST['dtven'];
        $preco          = $_POST['preco'];
        $desconto       = $_POST['desconto'];
        $acrescimo      = $_POST['acrescimo'];
        $posicao        = strpos($categoria, ",");
        $subcategoria   = substr($categoria,0,$posicao);
        $categoria      = substr($categoria,$posicao+1,strlen($categoria));

        if(isset($_POST['juros'])){
            $juros = $_POST['juros'];
        }else{
            $juros = '0';
        }
        $vltot          = $_POST['vltot'];

        $variaveis = [
            'id'            => $id,
            'tipo'          => $tipo,
            'id_usuario'    => $id_usuario,
            'id_categoria'  => $categoria,
            'dtemi'         => date("Y-m-d"),
            'dtven'         => $dtven,
            'preco'         => $preco,
            'desco'         => $desconto,
            'acres'         => $acrescimo,
            'juros'         => $juros,
            'vltot'         => $vltot,
            'id_subcategoria'  => $subcategoria
        ];

        if($id == null){
            $sql = "INSERT INTO movto (id,tipo,id_usuario,id_categoria,dtemi,dtven,preco,desco,acres,juros,vltot,id_subcategoria) VALUES (:id,:tipo,:id_usuario,:id_categoria,:dtemi,:dtven,:preco,:desco,:acres,:juros,:vltot,:id_subcategoria)";
        } else{
            $sql = "UPDATE movto set id_categoria = :id_categoria, dtemi = :dtemi, dtven = :dtven, preco = :preco, desco = :desco
            , acres = :acres, juros = :juros, vltot = :vltot, tipo = :tipo, id_usuario = :id_usuario, id_subcategoria = :id_subcategoria  where id = :id";
        }
        $pdo->prepare($sql)->execute($variaveis);
        
        echo "<script>alert('Cadastro realizado com sucesso ')</script>";
        if($tipo == '2'){
         echo "<script>window.location='lancamentoDespesa.php'</script>";   
        }else{
         echo "<script>window.location='lancamentoReceita.php'</script>";            
        }
    }
    else if(isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd"){
        $id = $_REQUEST["id"];
        try {
            $despesas = $pdo->prepare("SELECT * FROM movto WHERE id = ?");
            $despesas->bindParam(1, $id, PDO::PARAM_INT);
            if ($despesas->execute()) {
                $campos = $despesas->fetch(PDO::FETCH_OBJ);
                $id = $campos->id;
                $categoria = $campos->id_subcategoria;
                $dtven = $campos->dtven;
                $preco = $campos->preco;
                $desconto = $campos->desco;
                $acrescimo = $campos->acres;
                $juros = $campos->juros;
                $vltot = $campos->vltot;
            } else {
                throw new PDOException("Erro: Não foi possível executar a declaração sql");
            }
        } catch (PDOException $erro) {
            echo "Erro: ".$erro->getMessage();
    }
    }
     else if(isset($_REQUEST["act"]) && isset($_REQUEST["tipo"]) && $_REQUEST["act"] == "del"){
        $id = $_REQUEST["id"];
        $tipo = $_REQUEST["tipo"];
        try {
            $categorias = $pdo->prepare("DELETE FROM movto WHERE id = ?");
            $categorias->bindParam(1, $id, PDO::PARAM_INT);
            if ($categorias->execute()) {
                echo "<script>alert('Exclusão realizada com sucesso ')</script>";
                        if($tipo == '2'){
                             echo "<script>window.location='lancamentoDespesa.php'</script>";   
                            }else{
                             echo "<script>window.location='lancamentoReceita.php'</script>";            
                            }

            } else {
                throw new PDOException("Erro: Não foi possível executar a declaração sql");
            }
        } catch (PDOException $erro) {
            echo "Erro: ".$erro->getMessage();
    }
    }


?>