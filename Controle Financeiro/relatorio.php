<?php 
   session_start();
   
   include ("php/conexao.php");
   include ("php/index.php");
   include ("php/relatorio.php");
   require_once 'MercadoPago/lib/mercadopago.php';
   require_once 'php/PagamentoMP.php';
   $pagar = new PagamentoMP;
 
 
 //ID ref da fatura
 $id = $_SESSION['id'];
 // Iniciar busca        
 $valor = '5,00';
 $nome  = "Tudo Sob Controle";
 $url   = "http://tudosobcontrole.net.br";

 $btn   = $pagar->PagarMP($id , $nome , (float)$valor , $url);
   
   if(!isset($_SESSION['email']) || !isset($_SESSION['apelido'])){
       header("Location: login.html"); exit;
   }

   $date = date('H:i');
   $mesAtual = date("m") - 1;
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>TESTE</title>
      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <script src="code/highcharts.js"></script>
    <script src="code/modules/exporting.js"></script>
    <script src="code/modules/export-data.js"></script>

   </head>
   <body id="page-top">
      <!-- Page Wrapper -->
      <div id="wrapper">
         <!-- Sidebar -->
         <?php include ("menuLateral.php"); ?>
         <!-- End of Sidebar -->
      
         <!-- Begin Page Content -->
         <div class="container-fluid">
            <!-- Page Heading -->

            <!-- Content Row -->
            <div class="row">
               <!-- Area Chart -->
          <div class="col-md-12 text-right">
           <!-- <a class="btn btn-success" href="#">Finalizar</a> -->
           <?php echo $btn;?>
           
          </div>

                        <div class="col-xl-8 col-lg-7">
            
              <div id="comparativo" ></div>



    <script type="text/javascript">

var label = [500,100,150,200,250,300,350,400];
    Highcharts.chart('comparativo', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Receitas x Despesas'
    },
    xAxis: {
        categories: ['Janeiro', 'Fevereiro', 'Marco', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
    },
    yAxis: {
        title: {
            text: 'Total de Vendas'
        },
      labels:{
        formatter: function(){
          return 'R$ ' + this.value;
        }
      }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: true
        }
    },
    series: [
                { name: 'Receitas',
                 data: [<?php print "$receitasMes[0],$receitasMes[1],$receitasMes[2],$receitasMes[3],$receitasMes[4],$receitasMes[5],$receitasMes[6],$receitasMes[7],$receitasMes[8],$receitasMes[9],$receitasMes[10],$receitasMes[11]";?>]},
                 { name: 'Despesas',
                 data: [<?php print "$despesasMes[0],$despesasMes[1],$despesasMes[2],$despesasMes[3],$despesasMes[4],$despesasMes[5],$despesasMes[6],$despesasMes[7],$despesasMes[8],$despesasMes[9],$despesasMes[10],$despesasMes[11]";?>]}
                    ]
});
    </script>
            
          </div>
          <div class="col-xl-4 col-lg-5">
            
              <div id="categoria"></div>



    <script type="text/javascript">

   Highcharts.chart('categoria', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: false,
                    alpha: 55
                }
            },
            title: {
                text: 'Despesas por Categoria'
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 40
                }
            },
            series: [{
                name: 'Porcentagem ',
                data: [
                    <?php  $cont = 0; while($cont < sizeof($despesasCategoria)){
                         ?>[<?php $nome = $despesasCategoria[$cont][0];  print "'$nome'" ?>,<?php 
                         $valor = ($despesasCategoria[$cont][1] / $despesasAnual) * 100; $valor = number_format($valor,2);
         print "$valor"?>]
                            <?php if(sizeof($despesasCategoria) - $cont != 1){
                                ?>,<?php
                            }
         $cont++; }?>
                ]
            }]
        });
    </script>
            
          </div>
                     </div>
                     <br>
                     <!-- Card Body -->
                  </div>
               </div>
              
            <!-- Content Row -->
            <div class="row">
               <!-- Content Column -->
               <div class="col-lg-10 mb-4">
                  <!-- Project Card Example -->
                  <div class="card shadow mb-4">
                    
                     </div>
                  </div>
                  <!-- Color System -->
                  <div class="col-xl-4 col-lg-5">
            
              <div id="receitas"></div>



    <script type="text/javascript">

   Highcharts.chart('receitas', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: false,
                    alpha: 55
                }
            },
            title: {
                text: 'Receitas por Categoria'
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 40
                }
            },
            series: [{
                name: 'Porcentagem ',
                data: [
                    <?php  $cont = 0; while($cont < sizeof($receitasCategoria)){
                         ?>[<?php $nome = $receitasCategoria[$cont][0];  print "'$nome'" ?>,<?php 
                         $valor = ($receitasCategoria[$cont][1] / $receitaAnual) * 100; $valor = number_format($valor,2);
         print "$valor"?>]
                            <?php if(sizeof($receitasCategoria) - $cont != 1){
                                ?>,<?php
                            }
         $cont++; }?>
                ]
            }]
        });
    </script>
            
          </div>
                     </div>
            </div>
         </div>
         <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
         <div class="container my-auto">
            <div class="copyright text-center my-auto">
               <span>Copyright &copy; Your Website 2019</span>
            </div>
         </div>
      </footer>
      <!-- End of Footer -->
      </div>
      <!-- End of Content Wrapper -->
      </div>
      <!-- End of Page Wrapper -->
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
      </a>
      <!-- Bootstrap core JavaScript-->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/sb-admin-2.min.js"></script>
      <!-- Page level plugins -->
      <script src="vendor/chart.js/Chart.min.js"></script>
      <!-- Page level custom scripts -->
      <script src="js/demo/chart-area-demo.js"></script>
      <script src="js/demo/chart-pie-demo.js"></script>
   </body>
</html>