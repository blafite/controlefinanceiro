<!DOCTYPE html>
<html lang="pt">
  <head>
    <?php session_start(); 
    include("php/conexao.php"); 
    
    include("php/movtoCadastro.php");
      
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Planejamento Financeiro</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <!--Custom styles-->

    <script>

         function soma(){
         
         var preco        = Number(document.getElementById("preco").value);
         var desconto     = Number(document.getElementById("desconto").value);
         var acrescimo    = Number(document.getElementById("acrescimo").value);
         var juros        = Number(document.getElementById("juros").value);
         var descOption   = Number(document.getElementById("descOption").value);
         var acresOption  = Number(document.getElementById("acresOption").value);
         var jurosOption  = Number(document.getElementById("jurosOption").value);

         if(descOption == "2"){
            desconto = preco * (desconto/100);
         }
         
         if(acresOption == "2"){
            acrescimo = preco * (acrescimo/100);
         }
         
         if(jurosOption == "2"){
            juros = preco * (juros/100);
         }

        document.getElementById("vltot").value = preco + acrescimo + juros - desconto;
       }

    </script>


  </head>
  <body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
    <!-- Sidebar -->
    <?php include ("menuLateral.php"); ?>
    <!-- End of Sidebar -->
    <div class="container-fluid">
    <h1 class="h3 lg-12 text-gray-800">Lançamento de despesas</h1>
    <br>
    <form action="?act=save" method="POST">
      <div>
        <input type="number" class="form-control" placeholder="" name = "id_usuario" id= "id_usuario" style = "display:none" value = <?php  echo $_SESSION['id']; ?> >
        <input type="number" class="form-control" placeholder="" name = "tipo" id= "tipo" style = "display:none" value = "2" >
        <input type="text" name="id" style="display:none" value=<?php if($id) echo $id; ?> >
        Informe a categoria<br>
        <select id='categoria' name="categoria" class="form-control ">
          <option value = "">Selecione a Categoria</option>
          <?php 
            try {
                $categorias = $pdo->prepare("SELECT * FROM subcategoria where tipo = '2' and id_usuario = $_SESSION[id] order by DESCR");
                  if ($categorias->execute()) {
                      while ($campos = $categorias->fetch(PDO::FETCH_OBJ)) {
                          echo "<option value = ".$campos->id.",".$campos->id_categoria.""; if($categoria == $campos->id) echo ' selected'; echo ">".$campos->descr."</option>";
                      }
                  } else {
                      echo "Erro: Não foi possível recuperar os dados do banco de dados";
                  }
            } catch (PDOException $erro) {
              echo "Erro: ".$erro->getMessage();
            }
            ?>
        </select>
        <br>
        <div class="row">
          <div class="col-sm-6">
            Data do gasto
            <div class="input-group form-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
              </div>
              <input type="date" class="form-control" placeholder="Data do gasto" name = "dtven" id= "dtven" value = <?php echo $dtven; ?> >
            </div>
          </div>
          <div class="col-sm-6">
            Valor
            <input type="number" class="form-control" placeholder="" name = "preco" id= "preco" step="0.01" onBlur="soma()" value = <?php echo $preco; ?> >
          </div>
          <div class="col-sm-2">
            % ou R$
            <select id="descOption" class="form-control">
              <option value="1">R$</option>
              <option value="2">%</option>
            </select>
          </div>
          <div class="col-sm-2">
            Desconto
            <input type="number" class="form-control" placeholder="" name = "desconto" id= "desconto" step="0.01" onBlur="soma()" value = <?php echo $desconto; ?> >
          </div>
          <div class="col-sm-2">
            % ou R$
            <select id="acresOption" class="form-control">
              <option value="1">R$</option>
              <option value="2">%</option>
            </select>
          </div>
          <div class="col-sm-2">
            Acrescimo
            <input type="number" class="form-control" placeholder="" name = "acrescimo" id= "acrescimo" step="0.01" onBlur="soma()" value = <?php echo $acrescimo; ?> >
          </div>
          <div class="col-sm-2">
            % ou R$
            <select id="jurosOption" class="form-control">
              <option value="1">R$</option>
              <option value="2">%</option>
            </select>
          </div>
          <div class="col-sm-2">
            Juros
            <input type="number" class="form-control" placeholder="" name = "juros" id= "juros" step="0.01" onBlur="soma()" value = <?php echo $juros; ?> >
          </div>
        </div>
        <br>Valor Total
        <div class="input-group form-group">
          <div>
          </div>
        <input type="text" class="form-control" placeholder="" name = "vltot" id= "vltot" readonly value = <?php echo $vltot;?> >
        </div>
      </div>
      <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-6"> <button type="submit" class ="btn login_btn">Salvar</button></div>
      </div>
      <div class="form-group">
    </form>
    <br><br>
    <div class = "row">
    <table class="table table-striped">
    <thead>
    <tr>
    <th scope="col" style="display:none" name = "id">Id</th>
    <th scope="col" name = "descr">Despesas Lançadas</th>
    <th scope="col" name = "valor">Valor</th>
    <th scope="col" name = "data">Vencimento</th>
    </tr>
    </thead>
    <tbody>
    <?php
      // Bloco que realiza o papel do Read - recupera os dados e apresenta na tela
      try {
          $despesas = $pdo->prepare("SELECT movto.id,movto.vltot,subcategoria.descr,movto.dtven, movto.tipo FROM movto left join subcategoria on subcategoria.id = movto.id_subcategoria where movto.tipo = '2' and movto.id_usuario = $_SESSION[id] order by movto.ID desc");
              if ($despesas->execute()) {
                  while ($campos = $despesas->fetch(PDO::FETCH_OBJ)) {
                      echo "<tr>";
                      echo "<td style ='display:none'>".$campos->id."</td><td>".$campos->descr."</td><td>".$campos->vltot
                                 ."</td><td>".$campos->dtven."</td><td><center><a href=\"?act=upd&id=" . $campos->id . "\">[Alterar]</a>"
                                 ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                 ."<a href=\"?act=del&tipo="  . $campos->tipo . "&id=" . $campos->id . "\">[Excluir]</a></center></td>";
                      echo "</tr>";
                  }
              } else {
                  echo "Erro: Não foi possível recuperar os dados do banco de dados";
              }
      } catch (PDOException $erro) {
          echo "Erro: ".$erro->getMessage();
      }
      ?>
    </tbody>
    </table>
    </div>
    </div>
  </body>
</html>