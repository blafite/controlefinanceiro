<!DOCTYPE html>
<html lang="pt">
   <head>
      <?php session_start(); 
         include ("php/conexao.php"); 
         
         include("php/movtoCadastro.php");
           
         ?>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Planejamento Financeiro</title>
      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <!-- Custom styles for this template-->
      <link href="css/sb-admin-2.min.css" rel="stylesheet">

         <!-- Principal CSS do Bootstrap -->
    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Estilos customizados para esse template -->
<link href="pricing.css" rel="stylesheet">
  
   </head>

   <body id="page-top">
      <!-- Page Wrapper -->
      <div id="wrapper">
      <!-- Sidebar -->
      <?php include ("menuLateral.php"); ?>
      <!-- End of Sidebar -->
      <div class="a">
      <h1 class="h3 lg-12 text-gray-800">Configurações</h1>
      <br>
      <style>
	div.a{
		text-align: center;
	}
</style>


    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h4 class="display-4">Preços</h4>
      <p class="lead">Construa uma tabela de preços para seus potenciais clientes, com esse exemplo Bootstrap. Além do mais, é feito com componentes padrões, utilitários e um pouquinho de customização.</p>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Grátis</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mês</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>1 usuários</li>
              <li>2 GB de armazenamento</li>
              <li>Suporte por email</li>
              <li>Acesso ao centro de ajuda</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary">Cadastre-se de graça</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Pro</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$15 <small class="text-muted">/ mês</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>5 usuários</li>
              <li>10 GB de armazenamento</li>
              <li>Suporte por email prioritário</li>
              <li>Acesso ao centro de ajuda</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Conhecer</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Premium</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$29 <small class="text-muted">/ mês</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>10 usuários</li>
              <li>15 GB de armazenamento</li>
              <li>Suporte por email e telefone</li>
              <li>Acesso ao centro de ajuda</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Contate-nos</button>
          </div>
        </div>
        
      </div>
      <div class="a">
      <a class="btn btn-outline-primary" href="#">Salvar</a>
</div>

      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            <img class="mb-2" src="../../assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
          </div>

          

      </div>
      </div>
   </body>
</html>