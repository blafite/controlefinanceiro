<?php 
   include ("php/metas.php");
   session_start();
   
    ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Tudo Sob Controle</title>
      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css/sb-admin-2.min.css" rel="stylesheet">
      <style>
         div.a{
         text-align: center;
         }
      </style>
   </head>
   <body id="page-top">
      <!-- Page Wrapper -->
      <div id="wrapper">
      <!-- Sidebar -->
      <?php include ("menuLateral.php"); ?>
      <!-- End of Sidebar -->
      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
      </nav>
      <!-- End of Topbar -->
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <h1 class="h3 lg-12 text-gray-800">Metas</h1>
         <div class="container-fluid">
            <form action="?act=save" method="POST">
                <input type="text" name="id" style="display:none" value=<?php if($id) echo $id; ?> ><br>
                <input type="number" class="form-control" placeholder="" name = "id_usuario" id= "id_usuario" style = "display:none" value = <?php  echo $_SESSION['id']; ?> >
            <div class="form-row">
               <div class="col">
                  Selecione o tipo de Meta
                  <select class="browser-default custom-select" id="metaSelect" name="metaSelect">
                     <option selected>Selecione</option>
                     <option value="1">Curto Prazo</option>
                     <option value="2">Médio Prazo</option>
                     <option value="3">Longo Prazo</option>
                     <option value="4">Liberdade Financeira</option>
                  </select>
               </div>
               <div class="col">
                  Descrição da Meta
                  <input type="text" class="form-control" placeholder="" name="descr" id="descr">
               </div>
            </div>
            <br>
            Informe o periodo da Meta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor para cumprir a meta
            <div class="form-row">
               <div class="col-md-3">
                  <input type="number" class="form-control" placeholder="" name="quantMeta" id="quantMeta">
               </div>
               <div class="col-md-3">
                  <select class="browser-default custom-select" id="periodoMeta" name="periodoMeta">
                     <option selected value="1">Meses</option>
                     <option value="2">Anos</option>
                  </select>
               </div>
               <div class="col-md-6">
                  <div class="input-group mb-3">
                     <div class="input-group-prepend">
                        <span class="input-group-text">R$</span>
                     </div>
                     <input type="number" class="form-control" name="valor" id="valor" style="align:right;" aria-label="Quantia">
                     <div class="input-group-append">
                        <span class="input-group-text">.00</span>
                     </div>
                  </div>
               </div>
            </div>
            <center><button class="btn btn-outline-primary" type="submit ">Salvar Meta</button></center>
            </form>
         </div>
          <div class = "row">
        <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col" style="display:none" name = "id">Id</th>
      <th scope="col" name = "tipo">Tipo</th>
      <th scope="col" name = "descr">Descrição da Meta</th>
      <th scope="col" name = "periodo">Periodo</th>
      <th scope="col" name = "prazo">Conclusão</th>
      <th scope="col" name = "valor">Valor</th>
    </tr>
  </thead>
  <tbody>
  <?php
    // Bloco que realiza o papel do Read - recupera os dados e apresenta na tela
    try {
        $categorias = $pdo->prepare("SELECT * FROM metas where id_usuario = $_SESSION[id] order by ID");
            if ($categorias->execute()) {
                while ($campos = $categorias->fetch(PDO::FETCH_OBJ)) {
                    if($campos->tipo_meta == '1')  
                        $campos->tipo_meta = 'Curto Prazo';
                    else if($campos->tipo_meta == '2')   {
                        $campos->tipo_meta = 'Médio Prazo';
                    }
                    else if($campos->tipo_meta == '3')   {
                        $campos->tipo_meta = 'Longo Prazo';
                    }
                    else    {
                        $campos->tipo_meta = 'Liberdade Financeira';
                    }
                    echo "<tr>";
                    echo "<td style ='display:none'>".$campos->id."</td><td>".$campos->tipo_meta."</td><td>".$campos->descr
                               ."</td><td>".$campos->periodo."</td><td>".$campos->prazo."</td><td>R$ ".$campos->valor."</td><td><center><a href=\"?act=upd&id=" . $campos->id . "\">[Alterar]</a>"
                               ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               ."<a href=\"?act=del&id=" . $campos->id . "\">[Excluir]</a></center></td>";
                    echo "</tr>";
                }
            } else {
                echo "Erro: Não foi possível recuperar os dados do banco de dados";
            }
    } catch (PDOException $erro) {
        echo "Erro: ".$erro->getMessage();
    }
    ?>
  </tbody>
</table>
      </div>
      </div>
   </body>
</html>