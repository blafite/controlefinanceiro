<?php include ("php/categoriaCadastro.php"); 

session_start();?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tudo Sob Controle</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
 <?php include ("menuLateral.php"); ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">



        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
        <style>
	div.a{
		text-align: center;
	 }
     </style>
         <div>
         <div class="a">
      
      <h1 class="h3 lg-12 text-gray-800">Cadastro de Categoria</h1><br>
      <form action="?act=save" method="POST">
      <input type="number" class="form-control" placeholder="" name = "id_usuario" id= "id_usuario" style = "display:none" value = <?php  echo $_SESSION['id']; ?> >
      <div class="row">
      <div class="input-group form-group">
      <div class="col-sm-2">
      </div>
         <div class="col-sm-4">
         <h4> Selecione o tipo de categoria</h4><div></div>
          <input type="text" name="id" style="display:none" value=<?php if($id) echo $id; ?> ><br>
          <input type="radio" name="tipo" id="despesas" value="2" <?php if($tipo == '2') echo 'checked'; ?>  >Despesas
          <input type="radio" name="tipo" id="receitas" value="1" <?php if($tipo == '1') echo 'checked'; ?>  > Receitas 
          </div><br>
            <div class="col-sm-4">
              <h4>  Descrição da categoria</h4><br>
                <div class="input-group form-group">
              <div>
        </div>
      <input type="text" class="form-control" placeholder="" name = "descr" id= "descr" value=<?php echo "$descr";?>>
</div>
</div>




</div>
</div>
      <div class="a"> 
      <button class="btn btn-outline-primary" type="submit ">Salvar</button>
</div>
</div>
</div>
    <div class="form-group">


    
    </form>
    <br><br>
    <div class = "row">
        <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col" style="display:none" name = "id">Id</th>
      <th scope="col" name = "descr">Descrição da Categoria</th>
      <th scope="col" name = "tipo">Tipo</th>
    </tr>
  </thead>
  <tbody>
  <?php
    // Bloco que realiza o papel do Read - recupera os dados e apresenta na tela
    try {
        $categorias = $pdo->prepare("SELECT * FROM categoria where id_usuario = $_SESSION[id] order by ID");
            if ($categorias->execute()) {
                while ($campos = $categorias->fetch(PDO::FETCH_OBJ)) {
                    if($campos->tipo == '1')  
                        $campos->tipo = 'Receitas';
                    else {
                        $campos->tipo = 'Despesas';
                    }
                    echo "<tr>";
                    echo "<td style ='display:none'>".$campos->id."</td><td>".$campos->descr."</td><td>".$campos->tipo
                               ."</td><td><center><a href=\"?act=upd&id=" . $campos->id . "\">[Alterar]</a>"
                               ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               ."<a href=\"?act=del&id=" . $campos->id . "\">[Excluir]</a></center></td>";
                    echo "</tr>";
                }
            } else {
                echo "Erro: Não foi possível recuperar os dados do banco de dados";
            }
    } catch (PDOException $erro) {
        echo "Erro: ".$erro->getMessage();
    }
    ?>
  </tbody>
</table>
      </div>
       </div>

</body>
</html>