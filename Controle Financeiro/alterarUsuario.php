<!DOCTYPE html>
<html lang="pt">
   <head>
      <?php session_start(); 
         include ("php/conexao.php"); 
         
         include("php/movtoCadastro.php");
           
         ?>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Planejamento Financeiro</title>
      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <!-- Custom styles for this template-->
      <link href="css/sb-admin-2.min.css" rel="stylesheet">
  
   </head>

   <body id="page-top">
      <!-- Page Wrapper -->
      <div id="wrapper">
      <!-- Sidebar -->
      <?php include ("menuLateral.php"); ?>
      <!-- End of Sidebar -->
      <div class="a">
      <h1 class="h3 lg-12 text-gray-800">Alterar Usuário</h1>
      <br>
      <style>
	div.a{
		text-align: center;
	}
</style>
      <form action="?act=save" method="POST">
         <div>
         <div class="a">
 
             <div class="avatar_perfil">
          
                <img src="https://cdn0.iconfinder.com/data/icons/user-icon-profile-businessman-finance-vector-illus/100/19-1User-512.png" title="Perfil" width="200px" height="200px">
  
            </div>
        </div>
            <br>
            <div class="a">
            <div class="card-body">
				<form action="php/cadastro.php" method="POST">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control" placeholder="Nome" name = "nome" id= "nome">
						
                         </div>
                         <div class="input-group form-group">
                                   <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-mail-bulk"  ></i></span>
                                   </div>
                                   <input type="email" class="form-control" placeholder="E-mail" name = "email" id= "email">
                                   
							  </div>
							  
							  <div class="input-group form-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fas fa-meh"></i></span>
								</div>
								<input type="text" class="form-control" placeholder="Apelido" name = "apelido" id= "apelido">
								
						   </div>

                              <div class="input-group form-group">
                                        <div class="input-group-prepend">
                                             <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                        </div>
                                        <input type="number" class="form-control" placeholder="Telefone" name = "telefone" id= "telefone">
                                        
                                   </div>

                                   <div class="input-group form-group">
                                        <div class="input-group-prepend">
                                             <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" placeholder="Data de Nascimento" name = "datanascimento" id= "datanascimento">
                                        
                                   </div>

                                        <div class="input-group form-group">
                                                  <div class="input-group-prepend">
                                                       <span class="input-group-text"><i class="fas fa-key"></i></span>
                                                  </div>
                                                  <input type="password" class="form-control" placeholder="Senha" name = "senha" id ="senha">
                                             </div>

                                             <div class="input-group form-group">
						               <div class="input-group-prepend">
						            	<span class="input-group-text"><i class="fas fa-check-circle"></i></span>
					                	</div>
					            	<input type="password" class="form-control" placeholder="Confirme sua Senha" name = "senha" id ="senha">
					                    </div>
                                        
            <div class="col-md-12"> <button type="submit" class ="btn login_btn">Salvar</button></div>
         </div>
         <div class="form-group">
      </form>
      <br><br>
      <div class = "row">
</div>
      </div>
      </div>
   </body>
</html>