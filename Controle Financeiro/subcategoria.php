<!DOCTYPE html>
<html lang="pt">
   <head>
      <?php session_start(); 
         include ("php/conexao.php"); 
         
         include("php/subcategoriaCadastro.php");
           
         ?>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Tudo Sob Controle</title>
      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <!-- Custom styles for this template-->
      <link href="css/sb-admin-2.min.css" rel="stylesheet">
      </head>
   <body id="page-top">
      <!-- Page Wrapper -->
      <div id="wrapper">
      <!-- Sidebar -->
      <?php include ("menuLateral.php"); ?>
      <!-- End of Sidebar -->
      <style>
	div.a{
		text-align: center;
	 }
     </style>
         <div>
         <div class="a"> 
      <div class="container-fluid">
      <h1 class="h3 lg-12 text-gray-800">Cadastro de subcategorias</h1>
      <br>
      <form action="?act=save" method="POST">
      <div class="row">
      <div class="input-group form-group">
      <div class="col-sm-2">
      </div>
         <div class="col-sm-4">
         <input type="number" class="form-control" placeholder="" name = "id_usuario" id= "id_usuario" style = "display:none" value = <?php  echo $_SESSION['id']; ?> >
            <input type="text" name="id" style="display:none" value=<?php if($id) echo $id; ?> >
            Informe a categoria<br>
            <select id='categoria' name="categoria" class="form-control ">
               <option value = "">Selecione a Categoria</option>
               <?php 
                  try {
                      $categorias = $pdo->prepare("SELECT * FROM categoria where id_usuario = $_SESSION[id] order by DESCR");
                        if ($categorias->execute()) {
                            while ($campos = $categorias->fetch(PDO::FETCH_OBJ)) {
                                echo "<option value = ".$campos->id."," .$campos->tipo. ""; if($id_categoria == $campos->id) echo ' selected'; echo ">".$campos->descr."</option>";
                            }
                        } else {
                            echo "Erro: Não foi possível recuperar os dados do banco de dados";
                        }
                  } catch (PDOException $erro) {
                    echo "Erro: ".$erro->getMessage();
                  }
                  ?>
            </select>
            </div>
            <br>
            <div class="col-sm-4">
            Nome da Subcategoria
            <input type="text" class="form-control" placeholder="" name = "descr" id= "descr" value=<?php echo "$descr";?>>
            </div> 
            </div>
            </div><br>
      <div class="a">
      <button class="btn btn-outline-primary" type="subimt">Salvar</button>
</div>
</form>
            </div>
              <div class = "row">
        <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col" style="display:none" name = "id">Id</th>
      <th scope="col" name = "descr">Categoria</th>
      <th scope="col" name = "subdescr">SubCategoria</th>
      <th scope="col" name = "tipo">Tipo</th>
    </tr>
  </thead>
  <tbody>
  <?php
    // Bloco que realiza o papel do Read - recupera os dados e apresenta na tela
    try {
        $categorias = $pdo->prepare("SELECT subcategoria.id subid, subcategoria.descr subdescr, subcategoria.tipo, categoria.descr descr FROM subcategoria LEFT JOIN categoria ON subcategoria.id_categoria = categoria.id WHERE subcategoria.id_usuario = $_SESSION[id] ORDER BY subcategoria.ID");
            if ($categorias->execute()) {
                while ($campos = $categorias->fetch(PDO::FETCH_OBJ)) {
                    if($campos->tipo == '1')  
                        $campos->tipo = 'Receitas';
                    else {
                        $campos->tipo = 'Despesas';
                    }
                    echo "<tr>";
                    echo "<td style ='display:none'>".$campos->subid."</td><td>".$campos->descr."</td><td>".$campos->subdescr."</td><td>".$campos->tipo
                               ."</td><td><center><a href=\"?act=upd&id=" . $campos->subid . "\">[Alterar]</a>"
                               ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               ."<a href=\"?act=del&id=" . $campos->subid . "\">[Excluir]</a></center></td>";
                    echo "</tr>";
                }
            } else {
                echo "Erro: Não foi possível recuperar os dados do banco de dados";
            }
    } catch (PDOException $erro) {
        echo "Erro: ".$erro->getMessage();
    }
    ?>
  </tbody>
</table>
      </div>
            </div>
            </body>
</html>